pipeline{
    agent any
    environment {
        EC2_SERVER_IP = credentials('ec2-server-ip')
        IMAGE_NAME = ""
    }
    stages {
        stage('increment version') {
            steps{
                script {
                    echo 'incrementing app version...'
                    sh 'mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('build app') {
            steps {
                script {
                    echo 'building the app...'
                    sh 'mvn clean package'
                }
            }
        }
        stage('build image') {
            steps {
                script {
                    echo 'building the docker image...'
                    withCredentials([usernamePassword(credentialsId: 'docker-credential', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh "docker build -t gurkan96/maven-to-ec2-app:${IMAGE_NAME} ."
                        sh 'echo $PASS | docker login -u $USER --password-stdin'
                        sh "docker push gurkan96/maven-to-ec2-app:${IMAGE_NAME}"
                    }
                }
            }
        }
        stage('content-replace-for-correct-version') {
            steps {
                script {
                    echo "changing the app version in the docker-compose file."
                    contentReplace(
                      configs: [
                        fileContentReplaceConfig(
                          configs: [
                            fileContentReplaceItemConfig(
                              search: 'newversion',
                              replace: "${IMAGE_NAME}",
                              matchCount: 1,
                              verbose: false,
                            )
                          ],
                          fileEncoding: 'UTF-8',
                          lineSeparator: 'Unix',
                          filePath: 'docker-compose.yaml'
                        )
                      ]
                    )
                }
           }
        }
        stage('deploy') {
            steps {
                script {
                    echo 'deploying docker image to ec2 with docker-compose'
                    sshagent(['ec2-server-key']) {
                        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ec2-user@${EC2_SERVER_IP}:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no ec2-user@${EC2_SERVER_IP} docker-compose up -d"
                    }
                }
            }
        }
        stage('replace back') {
            steps {
                script {
                    echo "changing the app version in the docker-compose file."
                    contentReplace(
                      configs: [
                        fileContentReplaceConfig(
                          configs: [
                            fileContentReplaceItemConfig(
                              search: "${IMAGE_NAME}",
                              replace: "newversion",
                              matchCount: 1,
                              verbose: false,
                            )
                          ],
                          fileEncoding: 'UTF-8',
                          lineSeparator: 'Unix',
                          filePath: 'docker-compose.yaml'
                        )
                      ]
                    )
                }
           }
        }
        stage('commit version update after incrementing version'){
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credential', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config --global user.email "jenkins@gurkan.com" && ' +
                           'git config --global user.name "Jenkins" && ' +
                           'git remote set-url origin "https://${USER}:${PASS}@gitlab.com/gurkan96/java-maven-to-ec2-pipeline.git" && ' +
                           'git add . && ' +
                           'git commit -m "ci: version bump" && ' +
                           'git push origin HEAD:master'
                    }
                }
            }
        }
    }
}