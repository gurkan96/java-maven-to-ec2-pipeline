FROM openjdk:11-jre-slim

WORKDIR /usr/src/app

COPY target/*.jar ./app.jar

EXPOSE 8081

CMD ["java", "-jar", "app.jar"]